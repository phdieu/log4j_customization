# Log4J Customization
Demonstrates how you could write a Log Appender that can be enabled to be used by Axon.ivy. trough the `configurations/log4jconfig.xml` file.

## Integration
- The custom log appender must contribute the Axon.ivy core bundle `commons.lib`, that contains Log4j and its related libraries. Therefore this bundle registers itself as buddy of `commons.lib`. See [META-INF/MANIFEST.MF](https://github.com/ivy-samples/tomcatValve/blob/master/CustomLogAppender/META-INF/MANIFEST.MF) `Eclipse-RegisterBuddy: commons.lib`

## Installation
- Build this JAR via `mvn clean verify`. The jar is then create as `target/log4j_customization-XYZ-SNAPSHOT.jar`.
- Copy the JAR into the `dropins` directory of an Axon.ivy Engine or Designer
- Enable the appender in the log4jconfig.xml by adding a custom appender
```xml
  <appender name="LOG-DAILYROLL" class="uk.org.simonsite.log4j.appender.TimeAndSizeRollingAppender">
    <param name="File" value="${user.dir}/logs/ch.ivyteam.ivy.log"/>
    <param name="Threshold" value="WARN"/>
    <param name="DatePattern" value=".yyyy-MM-dd"/>
    <param name="MaxFileSize" value="10MB"/>
    <param name="MaxRollFileCount" value="100"/>
    <param name="ScavengeInterval" value="30000"/>
    <param name="BufferedIO" value="false"/>
    <param name="CompressionAlgorithm" value="GZ"/>
    <layout class="org.apache.log4j.PatternLayout">
      <param name="ConversionPattern" value="%-5p %-23d{ISO8601} [%t] %x: %c{1} - %m%n"/>
    </layout>
  </appender>
```
